For now the implement can meet the functional requirement, and the runtime complexity could be listed for each interface as following.

|           | best      | worst     | average   |
| :-----    | :----     | :----     | :----     |
| add       | O(logn)   | O(n)      | O(n)      | 
| remove    | O(n)      | O(n)      | O(n)      | 

Note : Because the items in the array need to be moved when trying to add a new item or delete an existing item, the worst runtime complexity could reach O(n)

Of course there is some optimization for the BinarySearchArray component, like the storage could be replace by another struct like BST, which could be replaced by the RB tree to get the best performance when the size of the tree is big enough, but that is just kind of hard for me to implement at the moment.