require_relative "BinarySearchArray.rb"

class RangeList
    
    def initialize()
        # key is left bound and value is right bound
        # can be regarded as a map from left to right, so call it left2right
        @left2right = BinarySearchArray.new
    end

    def add(range)
        key = range[0]
        value = range[1]
        puts "add([#{key},#{value}))"
        if key >= value
            # invalid
            return false
        end

        # remove those covered by the given range
        higher_key, higher_value = @left2right.higher_entry(key)
        while higher_value != nil && higher_value <= value
            @left2right.remove_key(higher_key)
            higher_key, higher_value = @left2right.higher_entry(key)
        end

        if higher_key != nil && higher_key <= value+1
            # merge to right
            @left2right.remove_key(higher_key)
            value = higher_value
        end

        floor_key, floor_value = @left2right.floor_entry(key)
        if floor_value != nil && floor_value >= key-1
            # merge to left
            key = floor_key
            value = [value, floor_value].max
        end

        @left2right.put(key, value)

    end

    def remove(range)
        key = range[0]
        value = range[1]
        puts "remove([#{key},#{value}))"
        if key >= value
            # invalid
            return false
        end

        # remove those coverd by the given range
        higher_key, higher_value = @left2right.higher_entry(key)
        while higher_value != nil && higher_value <= value
            @left2right.remove_key(higher_key)
            higher_key, higher_value = @left2right.higher_entry(key)
        end

        if higher_key != nil && higher_key <= value
            # split right range
            @left2right.remove_key(higher_key)
            @left2right.put(value, higher_value)
        end

        floor_key, floor_value = @left2right.floor_entry(key)
        if floor_value != nil && floor_value >= key
            # split left range
            # left part
            if key <= floor_key
                @left2right.remove_key(floor_key)
            else
                @left2right.put(floor_key, key)
            end
            
            if value < floor_value
                # right part
                @left2right.put(value, floor_value)
            end 
        end
    end

    def print
        keys = @left2right.keys
        values = @left2right.values
        row = ""
        for i in 0...@left2right.size do
            row += " [#{keys[i]}, #{values[i]}) "
        end
        puts(row)
    end
end

def test_cover_more_than_one()
    puts "test_cover_more_than_one"
    rl = RangeList.new
    rl.add([1, 2])
    rl.print
    # Should display: [1, 2)
    rl.add([4, 5])
    rl.print
    # Should display: [1, 2) [4, 5)
    rl.add([7, 8])
    rl.print
    # Should display: [1, 2) [4, 5) [7, 8)
    rl.add([1, 6])
    rl.print
    # Should display: [1, 8)
    puts "===================================="
end

def test_given_by_interviewer()
    puts "test_given_by_interviewer"
    rl = RangeList.new
    rl.add([1, 5])
    rl.print
    # Should display: [1, 5)
    rl.add([10, 20])
    rl.print
    # Should display: [1, 5) [10, 20)
    rl.add([20, 20])
    rl.print
    # Should display: [1, 5) [10, 20)
    rl.add([20, 21])
    rl.print
    # Should display: [1, 5) [10, 21)
    rl.add([2, 4])
    rl.print
    # Should display: [1, 5) [10, 21)
    rl.add([3, 8])
    rl.print
    # Should display: [1, 8) [10, 21)
    rl.remove([10, 10])
    rl.print
    # Should display: [1, 8) [10, 21)
    rl.remove([10, 11])
    rl.print
    # Should display: [1, 8) [11, 21)
    rl.remove([15, 17])
    rl.print
    # Should display: [1, 8) [11, 15) [17, 21)
    rl.remove([3, 19])
    rl.print
    # Should display: [1, 3) [19, 21)
    rl.remove([2, 20])
    rl.print
    # Should display: [1, 2) [20, 21)
    puts "===================================="
end


if __FILE__ == $0
    test_given_by_interviewer()
    test_cover_more_than_one()
end