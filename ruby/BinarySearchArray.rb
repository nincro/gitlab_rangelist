class BinarySearchArray
  def initialize
    @keys = []
    @values = []
    @size = 0
  end
  attr_accessor :keys, :values, :size

  def is_empty?
    return @size == 0
  end

  def put(key, value)
    i = _rank(key)
    if i < @size && @keys[i] == key
      @values[i] = value
    else
      j = @size
      while j > i
        @keys[j] = @keys[j - 1]
        @values[j] = @values[j - 1]
        j -= 1
      end

      @keys[i] = key
      @values[i] = value
      @size += 1
    end
  end
  
  def remove_key(key)
    i = _rank(key)
    return if @keys[i] != key
    for j in i..@size - 1
      @keys[j] = @keys[j + 1]
      @values[j] = @values[j + 1]
    end
    @keys[j + 1] = nil
    @values[j + 1] = nil
    @size -= 1
  end

  def min
    return @values[0]
  end
  def max
    return @values[-1]
  end
  
  # find the position of key in runtime complexity of O(logn)
  # return the index before which all the keys are smaller than the given key
  def _rank(key) 
    l = 0
    r = @size
    while l < r
      m = l + (r - l) / 2
      if @keys[m] == key
        # hit
        return m
      end
      if @keys[m] > key
        r = m
      else
        l = m + 1
      end
    end
    return l
  end

  # return the key which is not bigger than the given key (if exists)
  def floor_entry(key)
    i = _rank(key)
    if @keys[i] == key
      return @keys[i], @values[i]
    elsif i == 0
      return nil, nil
    else
      return @keys[i-1], @values[i-1]
    end
  end
  
  # return the key which is not smaller than the given key (if exists)
  def ceiling_entry(key)
    i = _rank(key)
    if i == @size
      return nil, nil
    end
    return @keys[i], @values[i]
  end

  # return the key which is higher than the given key (if exists)
  def higher_entry(key)
    i = _rank(key)
    if i == @size
      return nil, nil
    end
    if @keys[i] == key
      i+=1
    end
    if i == @size
      return nil, nil
    end
    return @keys[i], @values[i]
  end

  # return the key which is lower than the given key (if exists)
  def lower_entry(key)
    i = _rank(key)
    i -= 1
    if @keys[i] == key
      i -= 1
    end
    if i < 0
      return nil, nil
    end
    return @keys[i], @values[i]
  end

  def print
    row = ""
    for i in 0...@size
      row += "[#{@keys[i]}, #{@values[i]})"
    end
    puts row
  end
    
end

def test_lower_entry()
  puts "test_lower_entry"
  arr = BinarySearchArray.new()
  arr.put(2,5)
  arr.put(6,7)
  arr.print
  for i in 0..7 do 
    puts "arr.lower_entry(#{i}) #{arr.lower_entry(i)}"
  end
  puts "==============================="
end

def test_ceiling_entry()
  puts "test_ceiling_entry"
  arr = BinarySearchArray.new()
  arr.put(2,5)
  arr.put(6,7)
  arr.print
  for i in 0..7 do 
    puts "arr.ceiling_entry(#{i}) #{arr.ceiling_entry(i)}"
  end
  puts "==============================="
end

def test_higher_entry()
  puts "test_higher_entry"
  arr = BinarySearchArray.new()
  arr.put(2,5)
  arr.put(6,7)
  arr.print
  for i in 0..7 do 
    puts "arr.higher_entry(#{i}) #{arr.higher_entry(i)}"
  end
  puts "==============================="
end

def test_floor_entry()
  puts "test_floor_entry"
  arr = BinarySearchArray.new()
  arr.put(2,5)
  arr.put(6,7)
  arr.print
  for i in 0..7 do 
    puts "arr.floor_entry(#{i}) #{arr.floor_entry(i)}"
  end
  puts "==============================="
end

if __FILE__ == $0
  test_lower_entry()
  test_ceiling_entry()
  test_higher_entry()
  test_floor_entry()

end
