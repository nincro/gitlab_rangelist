For now the implement can meet the functional requirement, and the runtime complexity could be listed for each interface as following.

|        | best      | worst      | average   |
| :----- | :-------- | :--------- | :-------- |
| add    | O(log(n)) | O(nlog(n)) | O(log(n)) |
| remove | O(log(n)) | O(nlog(n)) | O(log(n)) |

Note : The storage of TreeMap used by RangeList is RB tree, so the runtime complexity could be reduced to O(log(n)) when trying to put or remove an entry from the TreeMap. But in the worst case we may need to remove all the range added before, so the complexity could reach O(nlog(n)), but all the range could at most be removed once, so the average runtime complexity should be O(log(n)).