package gitlab_rangelist.java;

public class Main {

    static void testGivenByInterviewer() {
        System.out.println("test_given_by_interviewer");
        RangeList rl = new RangeList();
        rl.add(new int[] { 1, 5 });
        rl.print();
        // Should display: [1, 5)
        rl.add(new int[] { 10, 20 });
        rl.print();
        // Should display: [1, 5) [10, 20)
        rl.add(new int[] { 20, 20 });
        rl.print();
        // Should display: [1, 5) [10, 20)
        rl.add(new int[] { 20, 21 });
        rl.print();
        // Should display: [1, 5) [10, 21)
        rl.add(new int[] { 2, 4 });
        rl.print();
        // Should display: [1, 5) [10, 21)
        rl.add(new int[] { 3, 8 });
        rl.print();
        // Should display: [1, 8) [10, 21)
        rl.remove(new int[] { 10, 10 });
        rl.print();
        // Should display: [1, 8) [10, 21)
        rl.remove(new int[] { 10, 11 });
        rl.print();
        // Should display: [1, 8) [11, 21)
        rl.remove(new int[] { 15, 17 });
        rl.print();
        // Should display: [1, 8) [11, 15) [17, 21)
        rl.remove(new int[] { 3, 19 });
        rl.print();
        // Should display: [1, 3) [19, 21)
        rl.remove(new int[] { 2, 20 });
        rl.print();
        // Should display: [1, 2) [20, 21)
        System.out.println("====================================");
    }

    public static void main(String[] args) {
        testGivenByInterviewer();
    }
}
