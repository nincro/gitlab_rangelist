package gitlab_rangelist.java;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class RangeList {

    TreeMap<Integer, Integer> left2right = new TreeMap<>();

    public boolean add(int[] range) {
        if (range.length < 2) {
            // corner case
            return false;
        }

        int key = range[0];
        int value = range[1];
        System.out.printf("add([%d,%d))\n", key, value);
        if (key >= value) {
            // invalid
            return false;
        }

        SortedMap<Integer, Integer> tm = left2right.tailMap(key);
        for (Map.Entry<Integer, Integer> higherEntry : tm.entrySet()) {
            if (higherEntry.getKey() > value + 1) {
                break;
            }
            // remove those covered by the given range
            if (higherEntry.getValue() <= value) {
                left2right.remove(higherEntry.getKey());
                continue;
            }
            // merge to right
            left2right.remove(higherEntry.getKey());
            value = higherEntry.getValue();
            break;
        }

        Map.Entry<Integer, Integer> fe = left2right.floorEntry(key);
        if (fe != null && fe.getValue() >= key - 1) {
            // merge to left
            key = fe.getKey();
            value = Math.max(value, fe.getValue());
        }

        left2right.put(key, value);
        return true;
    }

    public boolean remove(int[] range) {
        if (range.length < 2) {
            // corner case
            return false;
        }

        int key = range[0];
        int value = range[1];
        System.out.printf("remove([%d,%d))\n", key, value);
        if (key >= value) {
            // invalid
            return false;
        }

        SortedMap<Integer, Integer> tm = left2right.tailMap(key);
        for (Map.Entry<Integer, Integer> higherEntry : tm.entrySet()) {
            if (higherEntry.getKey() > value + 1) {
                break;
            }
            // remove those covered by the given range
            if (higherEntry.getValue() <= value) {
                left2right.remove(higherEntry.getKey());
                continue;
            }
            // split right range
            left2right.remove(higherEntry.getKey());
            left2right.put(value, higherEntry.getValue());
            break;
        }

        Map.Entry<Integer, Integer> fe = left2right.floorEntry(key);
        if (fe != null && fe.getValue() >= key) {
            // split left range
            // left part
            if (key <= fe.getKey()) {
                left2right.remove(fe.getKey());
            } else {
                left2right.put(fe.getKey(), key);
            }
            // right part
            if (value < fe.getValue()) {
                left2right.put(value, fe.getValue());
            }
        }
        return true;
    }

    public void print() {
        for (Map.Entry<Integer, Integer> entry : this.left2right.entrySet()) {
            System.out.printf("[%d,%d) ", entry.getKey(), entry.getValue());
        }
        System.out.println();
    }
}